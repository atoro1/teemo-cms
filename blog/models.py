import markdown
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.html import mark_safe
from python_vuetify_markdown import PythonVuetifyMarkdown
from taggit.managers import TaggableManager
from taggit.models import TaggedItemBase
from django.utils.text import slugify

STATUS = (
    (0, "Draft"),
    (1, "Publish")
)


class TaggedBlogPost(TaggedItemBase):
    content_object = models.ForeignKey('BlogEntry', on_delete=models.CASCADE)


class BlogEntry(models.Model):
    title = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100, unique=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='blog_posts')
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    published = models.DateTimeField(default=timezone.now)
    status = models.IntegerField(choices=STATUS, default=0)
    tags = TaggableManager(through=TaggedBlogPost)

    class Meta:
        ordering = ['-published']
        verbose_name_plural = "Blog entries"

    def get_body_as_markdown(self):
        return mark_safe(markdown.markdown(self.body, extensions=[PythonVuetifyMarkdown()]))

    def get_tags(self):
        return list(self.tags.names())

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super().save()
