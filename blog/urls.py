from django.urls import path
from .views import BlogListView, BlogEntryDetailView

urlpatterns = [
    path("", BlogListView.as_view(), name="blog-list-view"),
    path("<slug:slug>/", BlogEntryDetailView.as_view(), name="blog-entry-detail"),
]
