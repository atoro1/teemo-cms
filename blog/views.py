from django.utils import timezone
from django.views.generic import ListView, DetailView
from .models import BlogEntry


class BlogListView(ListView):
    model = BlogEntry
    paginate_by = 5
    paginate_orphans = 2
    allow_empty = True

    def get_queryset(self):
        return BlogEntry.objects.filter(published__lte=timezone.now(), status=1)


class BlogEntryDetailView(DetailView):
    model = BlogEntry

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['blog_content'] = self.object.get_body_as_markdown()
        context['next_entry'] = BlogEntry.objects.filter(
            published__gt=self.object.published, status=1).order_by('published').first()
        context['previous_entry'] = BlogEntry.objects.filter(
            published__lt=self.object.published, status=1).order_by('-published').first()
        context['first_entry'] = BlogEntry.objects.filter(status=1).order_by('published').first()
        context['latest_entry'] = BlogEntry.objects.filter(status=1).order_by('-published').first()
        return context
