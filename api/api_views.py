from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import Http404
from rest_framework import status
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from blog.models import BlogEntry
from gallery.models import Gallery, GalleryImage
from page.models import Page
from settings.models import Settings
from taggit.models import Tag
from .serializers import *
import logging
logger = logging.getLogger(__name__)

class SettingsDetail(APIView):
    """
    Retrieves settings if they are set, else a 404 to redirect to initial setup
    """
    def get_settings(self):
        try:
            return Settings.objects.all().first()
        except Settings.DoesNotExist:
            raise Http404

    def get(self, request, format=None):
        settings = self.get_settings()
        serializer = SettingsSerializer(settings)
        return Response(serializer.data)

    def put(self, request, format=None):
        settings = self.get_settings()
        serializer = SettingsSerializer(settings, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, format=None):
        # Create Super User object if passwords and emails match
        data = request.data
        if not data.get('password') == data.get('password2'):
            return Response('Passwords do not match!', status=status.HTTP_400_BAD_REQUEST)
        if not data.get('email') == data.get('email2'):
            return Response('Emails do not match!', status=status.HTTP_400_BAD_REQUEST)
        User.objects.create_superuser(data.get('username'), data.get('email'), data.get('password'))

        # Log in the user after creation
        user = authenticate(request, username=data.get('username'), password=data.get('password'))
        login(request, user)

        # Save into settings, serializer ignores extra fields
        serializer = SettingsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PageModelViewSet(ModelViewSet):
    """
    ViewSet for creating, updating, reading, and deleting Page objects
    """
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = Page.objects.all()
    serializer_class = PageSerializer


class GalleryModelViewSet(ModelViewSet):
    """
    ViewSet for creating, updating, reading, and deleting Gallery objects
    """
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = Gallery.objects.all()
    serializer_class = GallerySerializer


class GalleryImageModelViewSet(ModelViewSet):
    """
    ViewSet for creating, updating, reading, and deleting Gallery image objects
    """
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = GalleryImage.objects.all()
    serializer_class = GalleryImageSerializer


class GalleryImageSetApiView(APIView):
    """
    Returns a specific set of Gallery Images given the Gallery slug
    """

    def get_images(self, slug):
        images = GalleryImage.objects.filter(gallery__slug=slug)
        return images

    def get(self, request, slug):
        images = self.get_images(slug)
        serializer = GalleryImageSerializer(images, many=True)
        return Response(serializer.data)


class BlogEntryModelViewSet(ModelViewSet):
    """
    ViewSet for creating, updating, reading, and deleting blog entries
    """
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = BlogEntry.objects.all()
    serializer_class = BlogEntrySerializer


class TagModelViewSet(ModelViewSet):
    """
    ViewSet for creating, updating, reading, and deleting tags
    """
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = Tag.objects.all()
    serializer_class = TagSerializer


class LoginLogoutApiView(APIView):
    """
    Login and logout API
    """
    def get(self, request):
        logout(request)
        return Response("Logged out successfully")

    def post(self, request):
        user = authenticate(request,
                            username=request.data.get('username'),
                            password=request.data.get('password')
                            )
        if user is not None:
            login(request, user)
            return Response("Logged in successfully")
        return Response("The username and password you entered don't match", status=status.HTTP_401_UNAUTHORIZED)
