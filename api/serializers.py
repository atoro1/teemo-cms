from blog.models import BlogEntry
from gallery.models import Gallery, GalleryImage
from page.models import Page
from settings.models import Settings
from taggit.models import Tag
from rest_framework import serializers
from taggit_serializer.serializers import TagListSerializerField, TaggitSerializer
import logging
logger = logging.getLogger(__name__)

class SettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Settings
        fields = '__all__'


class PageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Page
        fields = '__all__'


class GallerySerializer(serializers.ModelSerializer):
    class Meta:
        model = Gallery
        fields = '__all__'


class GalleryImageSerializer(TaggitSerializer, serializers.ModelSerializer):
    tags = TagListSerializerField()
    class Meta:
        model = GalleryImage
        fields = '__all__'


class BlogEntrySerializer(TaggitSerializer, serializers.ModelSerializer):
    tags = TagListSerializerField()

    class Meta:
        model = BlogEntry
        exclude = ['slug']


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        exclude = ['slug']
