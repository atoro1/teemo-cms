from django.urls import path, include
from .api_views import *
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('page', PageModelViewSet)
router.register('gallery', GalleryModelViewSet)
router.register('gallery-image', GalleryImageModelViewSet)
router.register('blog', BlogEntryModelViewSet)
router.register('tag', TagModelViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('settings/', SettingsDetail.as_view()),
    path('images/<str:slug>/', GalleryImageSetApiView.as_view()),
    path('login/', LoginLogoutApiView.as_view()),
    path('logout/', LoginLogoutApiView.as_view()),
]
