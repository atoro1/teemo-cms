from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView, ListView
from gallery.models import GalleryImage, Gallery
from blog.models import BlogEntry
from taggit.models import Tag
import logging
logger = logging.getLogger(__name__)

class FirstRunView(TemplateView):
    template_name = "settings/first_run.html"


class PagesView(LoginRequiredMixin, TemplateView):
    template_name = "settings/pages.html"
    login_url = "/login/"


class ThemeView(LoginRequiredMixin, TemplateView):
    template_name = "settings/theme.html"
    login_url = "/login/"


class GalleryListView(LoginRequiredMixin, TemplateView):
    template_name = "settings/galleries.html"
    login_url = "/login/"


class GalleryEditView(LoginRequiredMixin, TemplateView):
    template_name = "settings/edit_gallery.html"
    login_url = "/login/"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['images'] = GalleryImage.objects.filter(gallery__slug=kwargs.get('slug')).order_by('-commission_date')
        context['gallery'] = Gallery.objects.get(slug=kwargs.get('slug'))
        context['allTags'] = [tag.name for tag in Tag.objects.all()]
        return context


class BlogEditListView(LoginRequiredMixin, ListView):
    template_name = 'settings/edit_blog.html'
    login_url = "/login/"

    paginate_by = 20
    paginate_orphans = 5
    allow_empty = True

    model = BlogEntry

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['allTags'] = list(Tag.objects.all().values_list('name', flat=True))
        return context


class TagEditView(LoginRequiredMixin, TemplateView):
    template_name = 'settings/edit_tags.html'
    login_url = '/login/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tags'] = [{'name': tag.name, 'id': tag.id} for tag in Tag.objects.all()]
        return context
