from django.urls import path
from .views import *

urlpatterns = [
    path('start/', FirstRunView.as_view()),
    path('pages/', PagesView.as_view(), name='pages-settings'),
    path('theme/', ThemeView.as_view(), name='theme-settings'),
    path('galleries/', GalleryListView.as_view(), name='gallery-list-settings'),
    path('galleries/<str:slug>/', GalleryEditView.as_view(), name='gallery-edit-settings'),
    path('blog/', BlogEditListView.as_view(), name='blog-edit-list'),
    path('tags/', TagEditView.as_view(), name='tag-edit-list'),
]