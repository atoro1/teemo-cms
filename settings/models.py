from django.db import models


def favicon_dir(instance, filename):
    return f"settings/favicon.{filename.split('.')[-1]}"


class Settings(models.Model):
    site_title = models.CharField(max_length=200)
    toolbar_title = models.CharField(max_length=200, blank=True, null=True)
    toolbar_icon = models.ImageField(upload_to='settings', blank=True, null=True)
    favicon = models.ImageField(upload_to=favicon_dir, blank=True, null=True)
    primary_color = models.CharField(max_length=7)
    secondary_color = models.CharField(max_length=7)
    accent_color = models.CharField(max_length=7)
    error_color = models.CharField(max_length=7)
    info_color = models.CharField(max_length=7)
    success_color = models.CharField(max_length=7)
    warning_color = models.CharField(max_length=7)
    toolbar_color = models.CharField(max_length=7, blank=True, null=True)
    background_color = models.CharField(max_length=7, blank=True, null=True)
    dark_mode = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = "Settings"

    def __str__(self):
        return "Site settings"
