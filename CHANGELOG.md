# Changelog

## 0.2.0 (2020-09-06)

**Notable changes:**

* IMPORTANT: There are new apps to add to settings.py's installed_apps. Compare yours to the new settings.py.dist
* Added new blog system
* Added new tagging and search system. Gallery images and blog entries may be tagged
* Added missing image title to image details page
* Updated requirements for packages to latest. Be sure to upgrade with new requirements.txt
* Updated framework to use CDN links for Vue, Vuetify, and other assets to keep up to date easier

## 0.1.3 (2020-02-02)

**Notable changes:**

* Added thumbnail preload so large images don't take too long for galleries
* Fixed click-to-exit for lightbox on gallery pages
* Fixed Esc-to-exit for lightbox on gallery pages
* Note: New changes require an extra entry in settings.py and another database migration

## 0.1.2 (2020-01-19)

**Notable changes:**

* Added OG tags for rich embeds on social media and chat apps
* Removing some extra, unused code
* Fixing non-working mobile menu, good to go now
* Changing size of some text on mobile

## 0.1.1 (2020-01-16)

**Notable changes:**

* Home page image now chosen randomly from Gallery images marked as 'Featured'
* Added documentation (readthedocs.io)
* Adding images will now refresh the page to show additions


## 0.1.0 (2020-01-12)

**Notable changes:**

* First release in completed state