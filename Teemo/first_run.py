from settings.models import Settings
from django.shortcuts import redirect


class SettingsRedirect:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        settings = Settings.objects.all().first()
        if not settings and not request.path.startswith("/settings/") and not request.path.startswith("/api/"):
            return redirect("/settings/start/")
            # pass
        elif settings and request.path == "/settings/start/":
            return redirect("/")

        response = self.get_response(request)
        return response
