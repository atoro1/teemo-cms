import random
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from gallery.models import GalleryImage
from page.models import Page


class HomeView(TemplateView):
    template_name = "home/home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        try:
            page = Page.objects.get(slug='home')
            context['page_content'] = page.get_body_as_markdown()
        except Page.DoesNotExist:
            # Creates a default page for the Home page if one does not exist. Will always
            # trigger after finishing first_run for new sites.
            page = Page.objects.create(
                title="Home",
                slug="home",
                root=True,
                order=0,
                body='<h1 class="text-h1 text-center text-md-left blue--text lighten-3 font-weight-medium">Welcome '
                     'to Teemo CMS</h1>\n\n<h4 class="text-h4 text-center text-md-left">Making showcasing your art '
                     'easier than ever!</h4>\n\nTeemo CMS was designed to make showing off your commissions, drawings, '
                     'and other art as easy as possible without the bulk and slowness of other, more general-purpose '
                     'CMS like WordPress. Some of our features include:\n\n* Speed and ease of use\n* Advanced options'
                     ' for advanced users\n* Highly customizable\n* The first art CMS made by and for furries\n\n'
                     '<p class="text-body-1 mt-6">Click on the cog in the bottom right to find the Pages settings,'
                     'where you can change this text to whatever you like.</p>\n\nBe sure to check out the '
                     'documentation for more help getting things set up\n\n<v-btn outlined large '
                     'href="https://teemocms.com">Teemo CMS Docs</v-btn>'
            )
            context['page_content'] = page.get_body_as_markdown()

        featured_images = GalleryImage.objects.filter(featured=True)
        if featured_images:
            context['featured'] = random.choice(featured_images)
        else:
            context['featured'] = False

        return context


class LogInView(TemplateView):
    template_name = "home/login.html"


class LogOutView(LoginRequiredMixin, TemplateView):
    template_name = "home/logout.html"
    login_url = '/login/'
