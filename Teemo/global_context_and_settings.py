from blog.models import BlogEntry
from gallery.models import Gallery
from page.models import Page
from settings.models import Settings


def global_context(request):
    settings = Settings.objects.first()
    data = {
        'galleries': Gallery.objects.order_by('order', 'name'),
        'pages': Page.objects.filter(root=True).order_by('order'),
        'settings': settings,
        'has_blog_entries': BlogEntry.objects.filter(status=1).count() > 0
    }

    return data
