from django.views.generic import TemplateView, DetailView
from .models import Page


class PageView(DetailView):
    template_name = "page/page_template.html"
    model = Page

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_content'] = self.object.get_body_as_markdown()
        return context
