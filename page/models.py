import markdown
from django.db import models
from django.utils.functional import cached_property
from django.utils.html import mark_safe
from python_vuetify_markdown import PythonVuetifyMarkdown


class Page(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    order = models.IntegerField(blank=True, null=True)
    root = models.BooleanField(default=True)
    dropdown = models.BooleanField(default=False)
    body = models.TextField(blank=True, null=True)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.title

    def get_body_as_markdown(self):
        return mark_safe(markdown.markdown(self.body, extensions=[PythonVuetifyMarkdown()]))

    @cached_property
    def children(self):
        return Page.objects.filter(parent=self).order_by("order")
