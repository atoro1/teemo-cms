from django.urls import path
from .views import PageView

urlpatterns = [
    path('<str:slug>/', PageView.as_view(), name="custom-page")
]
