# Generated by Django 3.0.2 on 2020-01-09 02:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('page', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='order',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='page',
            name='slug',
            field=models.SlugField(default=1),
            preserve_default=False,
        ),
    ]
