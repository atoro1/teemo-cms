from django.views.generic import DetailView, ListView
from taggit.models import Tag
from .models import Gallery, GalleryImage
import logging
logger = logging.getLogger(__name__)

class GalleryView(ListView):
    template_name = 'gallery/gallery.html'

    model = GalleryImage
    paginate_by = 16
    paginate_orphans = 4
    allow_empty = True

    context_object_name = 'images'

    def get_queryset(self):
        queryset = GalleryImage.objects.filter(gallery__slug=self.kwargs.get('slug')).order_by('-commission_date')
        if self.request.GET.get('q'):
            params = self.request.GET.get('q').split(',')
            for param in params:
                queryset = queryset.filter(tags__name=param)
        return queryset.distinct()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['random_image'] = GalleryImage.objects.filter(gallery__slug=self.kwargs.get('slug')).order_by('?').first()
        context['gallery'] = Gallery.objects.get(slug=self.kwargs.get('slug'))
        context['tags'] = [tag.name for tag in Tag.objects.all()]
        if self.request.GET.get('q'):
            context['search'] = self.request.GET.get('q').split(',')
        return context


class ImageView(DetailView):
    template_name = "gallery/image.html"
    model = GalleryImage

    def get_object(self, queryset=None):
        return GalleryImage.objects.get(pk=self.kwargs.get('pk'), gallery__slug=self.kwargs.get('gallery'))
