from django.db import models
from taggit.managers import TaggableManager
from taggit.models import TaggedItemBase


class Gallery(models.Model):
    name = models.CharField(max_length=20)
    slug = models.SlugField(unique=True)
    order = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.name} Gallery'

    class Meta:
        verbose_name_plural = 'galleries'


class TaggedImage(TaggedItemBase):
    content_object = models.ForeignKey('GalleryImage', on_delete=models.CASCADE)


class GalleryImage(models.Model):
    image = models.ImageField(upload_to='gallery-images')
    gallery = models.ForeignKey(Gallery, on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    artist = models.CharField(max_length=50)
    artist_link = models.URLField(blank=True, null=True)
    commission_date = models.DateField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    featured = models.BooleanField(default=False)
    order = models.IntegerField(default=0)
    tags = TaggableManager(through=TaggedImage)

    def __str__(self):
        return f'{self.gallery} - {self.title}'

    def get_tags(self):
        return list(self.tags.names())
