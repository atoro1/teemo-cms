from django.urls import path
from .views import GalleryView, ImageView

urlpatterns = [
    path('<slug:slug>/', GalleryView.as_view(), name='gallery'),
    path('<str:gallery>/<int:pk>/', ImageView.as_view(), name='image')
]
