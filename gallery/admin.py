from django.contrib import admin
from .models import GalleryImage, Gallery


@admin.register(GalleryImage)
class GalleryImageAdmin(admin.ModelAdmin):
    pass


@admin.register(Gallery)
class GalleryAdmin(admin.ModelAdmin):
    list_display = ('name', 'order')

