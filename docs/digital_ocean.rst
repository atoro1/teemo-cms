Digital Ocean setup
===================

Hosting an instance of Teemo CMS on Digital Ocean is easy.

Prerequisites
-------------

Head over to `Digital Ocean <https://www.digitalocean.com>`_ and make an account if you don't already have one.
Set up billing with a credit or debit card.


Creating a VPS
--------------

In the upper right, you'll see a green **Create** button.

.. image:: /images/do/do-1.jpg

Click this, and select the **Droplets** button from the dropdown.

It may seem overwhelming, since there are so many options and configurations to choose from, but I'll
make it easy. We want an **Ubuntu 18.04** OS, and we can manage with the smallest droplet size, since
the site won't use many resources until you start building a large gallery of images. You can always
upgrade later, so don't feel like you'll be stuck if you don't go big!

Select the following combination:

.. image:: /images/do/do-2.jpg

For the location, you can choose whichever is closest to yourself or your audience. Once that's done,
scroll down and click "Create Droplet".

You'll receive an email with your server password, you can use a program like Putty or MobaXTerm to
SSH into your server. The IP address for your server should be in the email and on the Digital Ocean
dashboard.


Installing software
-------------------

The server will come bare, so we'll need to install some software onto it to get it ready for Teemo.

We'll need a few things, so just copy and paste the following code into the SSH session once you're in,
one line at a time:

.. code-block:: shell

   sudo apt-update
   sudo apt-upgrade
   sudo apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget nginx
   cd /tmp
   wget https://www.python.org/ftp/python/3.8.1/Python-3.8.1.tgz
   tar -xf Python-3.8.1.tgz
   cd Python-3.8.1
   ./configure --enable-optimizations
   make -j 1
   sudo make altinstall

After all that, we need to make sure Python is installed and working

.. code-block:: shell

   python3.8 --version

You should see an output similar to the one below:

.. code-block:: shell

   Python 3.8.1

If you don't, something must have gone wrong at some point, try re-downloading Python with wget and starting
again.