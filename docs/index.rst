Welcome to Teemo CMS
=====================================

.. image:: /images/teemo-cms.png

Teemo CMS is a modern, easy to use, feature-rich content management system designed
around the needs of artists, commissioners, and other art-focused individuals.

**Features**

* Art-first focus
* Designed around ease-of-use for non-technical individuals
* Custom image galleries with a lightbox
* Rich embeds on platforms like Twitter, Discord, Facebook, and more
* Blog system with post scheduling feature
* Tag system to add tags to both blog entries and gallery images
* Custom pages with several options for designing

  * Markdown for simple, easy pages
  * Vue with Vuetify for advanced, feature-rich pages
  * Both can be used on each page

* Build on Django 3.0, so if you're comfortable you can modify it any way you need
* Documentation with simplicity in mind

  * Not a programmer? We don't expect you to be
  * Step-by-step instructions start to finish
  * Copy-and-paste code to get you running fast

.. toctree::
   :maxdepth: 3
   :caption: Documentation Contents:

   intro
   digital_ocean
   furhost


.. toctree::
   :maxdepth: 2
   :caption: Additional Information

   faq
   whats_new

If you still can't find what you're looking for, try in one of the following pages:

* :ref:`genindex`
* :ref:`search`
