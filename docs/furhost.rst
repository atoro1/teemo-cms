FurHost setup
=============

`FurHost <https://furhost.net>`_ is our own web host, owned and operated by the same team that created Teemo CMS.

The setup for Teemo CMS on FurHost is easier than a VPS, but has some minor limitations due to
being on shared hosting. If you're not technically savvy, or just want an easy solution, this is
a very good option, and the base plan is $5/month.


Setup
-----

Setup is very different for Teemo CMS on shared hosting. Once you sign up for shared hosting on FurHost,
simply email support and mention you want to install Teemo CMS. They'll get it set up and running based on
your information you provide.


Support
-------

For quick support, message Atoro on Discord, Telegram, or via email:

* Discord: **Atoro#0001**
* Telegram/Twitter: **@AtoroDesu**
* Email: **Atoro** (at) **furhost.net**