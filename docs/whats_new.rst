Changelog
=========

This page keeps a detailed, human-friendly rendering of what's new and changed
in specific versions.

v0.2.0
------

* IMPORTANT: There are new apps to add to settings.py's installed_apps. Compare yours to the new settings.py.dist
* Added new blog system
* Added new tagging and search system. Gallery images and blog entries may be tagged
* Added missing image title to image details page
* Updated requirements for packages to latest. Be sure to upgrade with new requirements.txt
* Updated framework to use CDN links for Vue, Vuetify, and other assets to keep up to date easier

v0.1.3
------

* Added thumbnail preload so large images don't take too long for galleries
* Fixed click-to-exit for lightbox on gallery pages
* Fixed Esc-to-exit for lightbox on gallery pages
* Note: New changes require an extra entry in settings.py and another database migration

v0.1.2
------

* Added OG tags for rich embeds on social media and chat apps
* Removing some extra, unused code
* Fixing non-working mobile menu, good to go now
* Changing size of some text on mobile

v0.1.1
------

* Home page image now chosen randomly from Gallery images marked as 'Featured'
* Added documentation (readthedocs.io)
* Adding images will now refresh the page to show additions

v0.1.0
------

* Initial release