Introduction
============

This is the documentation for Teemo CMS, a custom CMS for showcasing art.


Prerequisites
-------------

Teemo CMS is built on `Django <https://www.djangoproject.com/>`_, a web framework in Python. In this case, Teemo CMS
uses Python 3.8 or higher.

For this documentation, we are assuming an **Ubuntu 18.04** system.


Installing
----------

Teemo CMS is designed to be simple to use. However, it's a bit more complicated to get started. If you're
familiar with `Django <https://www.djangoproject.com/>`_, NGINX, Gunicorn, and systemctl services, you
shouldn't have much issue at all.

However, if you're not, or if you're new to this kind of thing entirely, we've written a second, much more
in-depth set of instructions for step-by-step guidance down in the :ref:`simple` section below.


Advanced
^^^^^^^^
You can pull the framework from GitLab:

.. code-block:: shell

   git clone https://gitlab.com/atoro1/teemo-cms.git

Once you've pulled the repo, create a Python virtual environment and install the requirements with pip.

.. code-block:: shell

   python3 -m venv teemo-venv
   source teemo-venv/bin/activate
   pip install -r requirements.txt

To get the site prepared to launch, there are still a few steps left. Bootstrapping the database (SQLite) and
getting your local settings.py files setup are key, as the site will not run without these in place.

Copy the Teemo/settings.dist.py into a new Teemo/settings.py and modify
the settings to meet your needs. The most important changes are:

* SECRET_KEY

  * Change this to a random 50 character key, using letters, numbers, and symbols.
    Don't commit this to your repo if you're doing your own version control.

* ALLOWED_HOSTS

  * This is an array of domains that point to the site, required to work in production.

* Security settings

  * At the bottom, there are several security setting options you can change.
    The defaults are alright, but you can enable HSTS through Django here if you don't
    have it enabled through your reverse proxy or Cloudflare, for example.

After your settings are in place, we need to get the database set up. Just run this command and it's good to go.

.. code-block:: shell

   python manage.py migrate

Once that's complete, you can run the site by using ``python manage.py runsite`` for a local instance.
If you know how to use and set up NGINX, Gunicorn, and systemctl scripts, you're all set. If you need some
pointers, or a clear example of a working setup, check out the Simple section below for more details.


.. _simple:

Simple
^^^^^^

When we said this was a step-by-step, we meant it. This guide is from step 0 all the way to step 99.
A large number of people can probably skip the first few sections, but detail is key for non-technical
people, so this guide is detailed.


Get a domain
~~~~~~~~~~~~

Every website needs an actual domain to access it. Our recommendation is `NameSilo <https://www.namesilo.com>`_.
Simple step, probably the easiest one.


Set up CloudFlare
~~~~~~~~~~~~~~~~~

While this isn't a 100% necessary step, it makes many things easier, faster, and more secure.

Create an account on `CloudFlare <https://www.cloudflare.com>`_ if you don't already have one. Add the domain
you just purchased, following the steps displayed. One of the steps will ask you to change the DNS settings
to point the domain to CloudFlare.

   Go to the `domain manager <https://www.namesilo.com/account_domains.php>`_ page on NameSilo
   Click on your domain name
   Scroll down to the NameServers block, and click the **Change** link
   Replace the current nameservers with the ones CloudFlare provides in the setup
   Click Submit

We can leave the DNS records for now, we'll change those later after we choose a place to host the site.


Get a place to host the website
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Websites don't magically appear on the internet, they need a place to live. For Teemo CMS, you'll need
either a :abbr:`VPS (Virtual Private Server)`, or a web host that is able to host Python applications.

The best bet for a VPS if you want to go that route, is `Digital Ocean <https://www.digitalocean.com>`_.
They offer a $5/month 'Droplet' server that can run Teemo CMS without issues. Follow
:doc:`/digital_ocean` if you want a step-by-step guide on getting a Digital Ocean Droplet set up for Teemo CMS.

If you want to go the easy route with a web host, `FurHost <https://furhost.net>`_ is our own web host
and supports Django out of the box. Hosting is via Plesk, which means easy and free SSL, email addresses,
and more. Same price, starting at $5/month, with personal help available to  get you up and running easier.


Teemo CMS Setup
~~~~~~~~~~~~~~~

If you went the VPS route, you'll need to pull the code for Teemo CMS from GitLab.
We need to create a directory for the site to live in, get the database set up, and set up the virtual
environment for Python. SSH into your server and enter the following commands:

.. code-block:: shell

   cd ~
   mkdir Teemo
   cd Teemo
   git clone https://gitlab.com/atoro1/teemo-cms.git .
   python3.8 -m venv teemo-venv
   source teemo-venv/bin/activate
   pip install -r requirements.txt

There is one file that need to be changed to run the site. Once you've got your hosting situation figured
out, go into the **Teemo** directory inside the site directory. Look for ``settings.dist.py``.
Either copy the file and remove the ``.dist`` from the filename, or just rename the existing file.

In the settings.py, on line 6, change the SECRET_KEY value to a random 50 character key, with letters,
numbers, and symbols. For an easy solution, go to `<https://djecrety.ir/>`_ to generate a random key.

On line 12, add your domain to the list of ALLOWED_HOSTS. It's a list of strings, and we need to include
both the plain (example.com) and full (www.example.com) versions.

.. code-block:: python

   ALLOWED_HOSTS = ['example.com', 'www.example.com']

With that done, save the file and exit.


Database
~~~~~~~~

The site needs a database to save your information, lucky for you this is a single command to get everything up and
running. Just copy and paste the following into the SSH window to bootstrap the database:

.. code-block:: shell

    python manage.py migrate


NGINX, Gunicorn, and Systemctl
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This is one of the easier bits, mostly because you can copy and paste the code, only changing small
bits that are unique to your system, like directory names, venv names, and the URLs you have.

In the SSH session, type the following:

.. code-block:: shell

   sudo nano /etc/systemd/system/teemo.service

Copy and paste the following code into the file (:kbd:`Shift + Insert`), and change the **User** to your SSH username,
the **WorkingDirectory** and **ExecStart** to match your directory structure.

.. code-block:: shell

   [Unit]
   Description=Teemo CMS Gunicorn daemon
   Requires=teemo.socket
   After=network.target

   [Service]
   User=YOUR_USERNAME
   Group=www-data
   WorkingDirectory=/home/YOUR_USERNAME/Teemo
   ExecStart=/home/YOUR_USERNAME/Teemo/teemo-venv/bin/gunicorn \
             --log-level info \
             --access-logfile - \
             --workers 5 \
             --bind unix:/run/teemo.sock \
             --worker-class gevent \
             --worker-connections 1000 \
             --timeout 120 \
             Teemo.wsgi:application

   [Install]
   WantedBy=multi-user.target

Use :kbd:`Control-x` to exit, and hit :kbd:`y` to save when it asks. Next, enter the following into the SSH session:

.. code-block:: shell

   sudo nano /etc/systemd/system/teemo.socket

Copy and paste the following code into the file:

.. code-block:: shell

   [Unit]
   Description=Teemo CMS Gunicorn socket

   [Socket]
   ListenStream=/run/teemo.sock

   [Install]
   WantedBy=sockets.target

Same as before, :kbd:`Control-x` to exit, and :kbd:`y` to save. Finally, the last bit for NGINX:

.. code-block:: shell

   sudo nano /etc/nginx/sites-available/teemo.conf

Copy and paste the following code into the file, changing the **server_name** to your URLs for the site,
and the **location /static/** block to your  directory structure:

.. code-block:: shell

   server {
       listen 80;
       server_name teemocms.com www.teemocms.com;
       client_max_body_size 100M; # You can change this to increase the file upload size limit

       location = /favicon.ico { access_log off; log_not_found off; }
       location /static/ {
           root /home/YOUR_USERNAME/Teemo;
       }
       location /media/ {
           root /home/YOUR_USERNAME/Teemo;
       }

       location / {
           include proxy_params;
           proxy_pass http://unix:/run/teemo.sock;
       }
   }

Again, :kbd:`Control-x` to exit, and :kbd:`y` to save. We need to copy this file into NGINX's "sites-enabled" directory next,
so just copy and paste the following:

.. code-block:: shell

   sudo cp /etc/nginx/sites-available/teemo.conf /etc/nginx/sites-enabled/


Any time the teemo.conf in sites-available is changed, it needs to be copied to the sites-enabled directory
and NGINX needs to be restarted.

Once the file is copied, it's time to get everything running. We've made a lot of changes, so we need
to restart a few services on the server, like **NGINX**, **gunicorn**, and the **socket** service.

.. code-block:: shell

   sudo systemctl daemon-reload
   sudo systemctl start teemo.socket
   sudo systemctl start teemo.service
   sudo systemctl restart nginx

In theory, everything should be up and running, and you can access your website on your domain now!