.. _faq:

Frequently Asked Questions
==========================

Frequently asked questions. Everybody will have questions as they go through
something new, so this is here to help answer the ones we get most often.


.. contents:: Questions
    :local:

Software
--------

Questions regarding the software the site runs on belong here.

What is Django?
~~~~~~~~~~~~~~~

Django is a flexible web framework built in the Python programming language. It's a 'batteries included'
framework, meaning a lot of the hard work that goes into building a site (like authentication, sessions,
security, and routing) are all already done for you. It's absolutely not an easy, one-click thing like
WordPress, but it makes creating custom websites much easier.